package top.hmtools.wxmp.message.template.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class SendTemplateMessageResult extends ErrcodeBean {

	private Long msgid;

	public Long getMsgid() {
		return msgid;
	}

	public void setMsgid(Long msgid) {
		this.msgid = msgid;
	}

	@Override
	public String toString() {
		return "SendTemplateMessageResult [msgid=" + msgid + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
