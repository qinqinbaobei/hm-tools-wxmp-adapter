package top.hmtools.wxmp.message.group.model.preview;

public class MpvedioPreviewParam extends BasePreviewParam {

	
	private MediaId mpvideo;

	public MediaId getMpvideo() {
		return mpvideo;
	}

	public void setMpvideo(MediaId mpvideo) {
		this.mpvideo = mpvideo;
	}

	@Override
	public String toString() {
		return "MpvedioPreviewParam [mpvideo=" + mpvideo + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
	
	
}
