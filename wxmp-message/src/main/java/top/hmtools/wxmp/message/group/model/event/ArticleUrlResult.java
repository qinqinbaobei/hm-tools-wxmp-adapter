package top.hmtools.wxmp.message.group.model.event;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ArticleUrlResult {

	@XStreamAlias(value = "Count")
	private Integer count;
	
	@XStreamAlias(value = "ResultList")
	private List<ArticleUrlResultItem> resultList;

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<ArticleUrlResultItem> getResultList() {
		return resultList;
	}

	public void setResultList(List<ArticleUrlResultItem> resultList) {
		this.resultList = resultList;
	}

	
}
