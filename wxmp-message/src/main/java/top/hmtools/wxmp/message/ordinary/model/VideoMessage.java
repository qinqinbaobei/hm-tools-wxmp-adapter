package top.hmtools.wxmp.message.ordinary.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 视频消息
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[fromUser]]></FromUserName>
	<CreateTime>1357290913</CreateTime>
	<MsgType><![CDATA[video]]></MsgType>
	<MediaId><![CDATA[media_id]]></MediaId>
	<ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
	<MsgId>1234567890123456</MsgId>
</xml>
 * }
 * @author HyboWork
 *
 */
@WxmpMessage(msgType=MsgType.video)
public class VideoMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6904849231273452154L;

	/**
	 * 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	@XStreamAlias("MediaId")
	private String mediaId;
	
	/**
	 * 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	@XStreamAlias("ThumbMediaId")
	private String thumbMediaId;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getThumbMediaId() {
		return thumbMediaId;
	}

	public void setThumbMediaId(String thumbMediaId) {
		this.thumbMediaId = thumbMediaId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "VideoMessage [mediaId=" + mediaId + ", thumbMediaId=" + thumbMediaId + ", toUserName=" + toUserName
				+ ", fromUserName=" + fromUserName + ", createTime=" + createTime + ", msgType=" + msgType + ", msgId="
				+ msgId + "]";
	}
	
	
}
