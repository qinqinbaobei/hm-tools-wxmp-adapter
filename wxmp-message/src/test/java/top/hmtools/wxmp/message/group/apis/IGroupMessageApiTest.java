package top.hmtools.wxmp.message.group.apis;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.message.BaseTest;
import top.hmtools.wxmp.message.group.model.Articles;
import top.hmtools.wxmp.message.group.model.DeleteParam;
import top.hmtools.wxmp.message.group.model.GroupMessageSendStatusParam;
import top.hmtools.wxmp.message.group.model.GroupMessageSendStatusResult;
import top.hmtools.wxmp.message.group.model.SpeedParam;
import top.hmtools.wxmp.message.group.model.SpeedResult;
import top.hmtools.wxmp.message.group.model.UploadImageParam;
import top.hmtools.wxmp.message.group.model.UploadImageResult;
import top.hmtools.wxmp.message.group.model.UploadNewsParam;
import top.hmtools.wxmp.message.group.model.UploadNewsResult;
import top.hmtools.wxmp.message.group.model.openIdGroupSend.OpenIdGroupSendResult;
import top.hmtools.wxmp.message.group.model.openIdGroupSend.TextOpenIdGroupSendParam;
import top.hmtools.wxmp.message.group.model.preview.PreviewResult;
import top.hmtools.wxmp.message.group.model.preview.TextPreviewParam;
import top.hmtools.wxmp.message.group.model.tagGroupSend.Filter;
import top.hmtools.wxmp.message.group.model.tagGroupSend.TagGroupSendResult;
import top.hmtools.wxmp.message.group.model.tagGroupSend.TagMsgType;
import top.hmtools.wxmp.message.group.model.tagGroupSend.Text;
import top.hmtools.wxmp.message.group.model.tagGroupSend.TextTagGroupSendParam;

public class IGroupMessageApiTest extends BaseTest{
	
	private IGroupMessageApi groupMessageApi;

	@Test
	public void testUploadImage() {
		UploadImageParam uploadImageParam = new UploadImageParam();
		uploadImageParam.setMedia(new File("E:\\tmp\\aaaa.png"));
		UploadImageResult result = this.groupMessageApi.uploadImage(uploadImageParam);
		this.printFormatedJson("群发接口和原创校验--1 上传图文消息内的图片获取URL【订阅号与服务号认证后均可用】", result);
		
//		{
//			  "errcode" : 0,
//			  "url" : "http://mmbiz.qpic.cn/mmbiz_png/OibIGxsavoIMY5gRcicHswUIpGx9teD9xSiaqiaBoL46IZM4f7kNAm7XcW1gbu4icr07zDBvyOGT1v4ZibmFib8MUZfNw/0"
//			}
	}

	@Test
	public void testUploadNews() {
		UploadNewsParam uploadNewsParam = new UploadNewsParam();
		List<Articles> articles = new ArrayList<>();
		Articles articleAA = new Articles();
		articleAA.setAuthor("嗨啵");
		articleAA.setContent("这只是测色，I Love xiao wan");
		articleAA.setContent_source_url("http://www.hybomyth.com");
		articleAA.setDigest("aaaaaaa");
		articleAA.setNeed_open_comment(1);
		
		articleAA.setOnly_fans_can_comment(0);
		articleAA.setShow_cover_pic(1);
		//上传临时图片素材返回的 media_id
		articleAA.setThumb_media_id("XR22xvZrOYzkgfAggC5GnV4nFPybKxpY-96kc83aplMG7CfZrZ_hZzN5Xj1GElL8");
		articleAA.setTitle("伟大的嗨啵");
		
		articles.add(articleAA);
		
		uploadNewsParam.setArticles(articles);
		UploadNewsResult result = this.groupMessageApi.uploadNews(uploadNewsParam );
		this.printFormatedJson("群发接口和原创校验--2 上传图文消息素材【订阅号与服务号认证后均可用】", result);
	}

	@Test
	public void testTagGroupMessageSend() {
		TextTagGroupSendParam tagGroupSendParam = new TextTagGroupSendParam();
		Filter filter = new Filter();
		filter.setIs_to_all(true);
		tagGroupSendParam.setFilter(filter );
		Text text = new Text();
		text.setContent("哈哈，这是指定群发");
		tagGroupSendParam.setText(text );
		tagGroupSendParam.setMsgtype(TagMsgType.text);
		TagGroupSendResult result = this.groupMessageApi.tagGroupMessageSend(tagGroupSendParam );
		this.printFormatedJson("群发接口和原创校验--3 根据标签进行群发【订阅号与服务号认证后均可用】", result);
	}

	@Test
	public void testOpenIdGroupMessageSend() {
		TextOpenIdGroupSendParam openIdGroupSendParam = new TextOpenIdGroupSendParam();
		openIdGroupSendParam.setMsgtype(TagMsgType.text);
		top.hmtools.wxmp.message.group.model.openIdGroupSend.Text text = new top.hmtools.wxmp.message.group.model.openIdGroupSend.Text();
		text.setContent("ohohohoho");
		openIdGroupSendParam.setText(text );
		List<String> touser = new ArrayList<>();
		touser.add("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		touser.add("o-OZ0v4gDjwgLPPWJElm8jkvrnRo");
		openIdGroupSendParam.setTouser(touser );
		OpenIdGroupSendResult result = this.groupMessageApi.openIdGroupMessageSend(openIdGroupSendParam );
		this.printFormatedJson("群发接口和原创校验--4 根据OpenID列表群发【订阅号不可用，服务号认证后可用】", result);
	}

	@Test
	public void testDeleteGroupMessageSended() {
		DeleteParam deleteParam = new DeleteParam();
		deleteParam.setArticle_idx(1);
		deleteParam.setMsg_id(222);
		ErrcodeBean result = this.groupMessageApi.deleteGroupMessageSended(deleteParam );
		this.printFormatedJson("群发接口和原创校验--5 删除群发【订阅号与服务号认证后均可用】", result);
	}

	/**
	 * 微信手机客户端成功收到消息
	 */
	@Test
	public void testPreviewGroupMessageSend() {
		TextPreviewParam basePreviewParam = new TextPreviewParam();
		basePreviewParam.setMsgtype(TagMsgType.text);
		top.hmtools.wxmp.message.group.model.preview.Text text = new top.hmtools.wxmp.message.group.model.preview.Text();
		text.setContent("haha");
		basePreviewParam.setText(text );
		basePreviewParam.setTouser("o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		PreviewResult result = this.groupMessageApi.previewGroupMessageSend(basePreviewParam );
		this.printFormatedJson("群发接口和原创校验--6 预览接口【订阅号与服务号认证后均可用】", result);
	}

	@Test
	public void testGetGroupMessageSendStatus() {
		GroupMessageSendStatusParam groupMessageSendStatusParam = new GroupMessageSendStatusParam();
		groupMessageSendStatusParam.setMsg_id("111");
		GroupMessageSendStatusResult result = this.groupMessageApi.getGroupMessageSendStatus(groupMessageSendStatusParam );
		this.printFormatedJson("群发接口和原创校验--7 查询群发消息发送状态【订阅号与服务号认证后均可用】", result);
	}

	@Test
	public void testGetGroupMessageSendSpeed() {
		SpeedResult result = this.groupMessageApi.getGroupMessageSendSpeed();
		this.printFormatedJson("控制群发速度--获取群发速度", result);
	}

	@Test
	public void testSetGroupMessageSendSpeed() {
		SpeedParam speedParam = new SpeedParam();
		speedParam.setSpeed(2);
		ErrcodeBean result = this.groupMessageApi.setGroupMessageSendSpeed(speedParam );
		this.printFormatedJson("控制群发速度--设置群发速度", result);
	}

	@Override
	public void initSub() {
		this.groupMessageApi = this.wxmpSession.getMapper(IGroupMessageApi.class);
	}

}
