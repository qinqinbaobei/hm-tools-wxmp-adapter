package top.hmtools.dynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

public class MapperProxy implements InvocationHandler {
	
	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	// 生成某个接口的mapper
	public  <T> T newInstance(Class<T> clazz) {
		MapperProxy proxy = new MapperProxy();
		// 动态代理
		return (T) Proxy.newProxyInstance(clazz.getClassLoader(), new Class[] { clazz }, proxy);
	}

	// 调用mapper方法时实际执行的内容
	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		System.out.println("invoke " + method.getName());
		System.out.println(method);
		Class<?> returnType = method.getReturnType();
//		this.logger.info("proxy：{}",proxy);
//		this.logger.info("method：{}",method);
//		this.logger.info("args：{}",args);
		System.out.println("args：");
		System.out.println(JSON.toJSONString(args));
		
		Map<String,String> result = new HashMap<>();
		result.put("ttt", "");
		String jsonString = JSON.toJSONString(result);
		return JSON.parseObject(jsonString, returnType);
	}
}
