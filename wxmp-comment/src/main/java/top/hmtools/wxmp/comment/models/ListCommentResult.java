package top.hmtools.wxmp.comment.models;

import java.util.List;

import top.hmtools.wxmp.core.model.ErrcodeBean;

/**
 * Auto-generated: 2019-08-21 22:16:6
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ListCommentResult extends ErrcodeBean {

	private List<Comment> comment;

	/**
	 * 总数，非comment的size around
	 */
	private int total;

	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}

	public List<Comment> getComment() {
		return comment;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getTotal() {
		return total;
	}

	@Override
	public String toString() {
		return "ListCommentResult [comment=" + comment + ", total=" + total + ", errcode=" + errcode + ", errmsg="
				+ errmsg + "]";
	}

}