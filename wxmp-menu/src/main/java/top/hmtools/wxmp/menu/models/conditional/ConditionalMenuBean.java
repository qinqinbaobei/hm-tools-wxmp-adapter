package top.hmtools.wxmp.menu.models.conditional;

import top.hmtools.wxmp.menu.models.simple.MenuBean;

/**
 * 个性自定义菜单目录
 * @author Hybomyth
 *
 */
public class ConditionalMenuBean extends MenuBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2323093282995341309L;
	
	/**
	 * 个性化菜单id
	 */
	private String menuid;

	/**
	 * 个性自定义菜单匹配规则
	 */
	private MatchruleBean matchrule;

	/**
	 * 个性自定义菜单匹配规则
	 * @return
	 */
	public MatchruleBean getMatchrule() {
		return matchrule;
	}

	/**
	 * 个性自定义菜单匹配规则
	 * @param matchrule
	 */
	public void setMatchrule(MatchruleBean matchrule) {
		this.matchrule = matchrule;
	}
	
	

	public String getMenuid() {
		return menuid;
	}

	public void setMenuid(String menuid) {
		this.menuid = menuid;
	}

	@Override
	public String toString() {
		return "ConditionalMenuBean [menuid=" + menuid + ", matchrule=" + matchrule + ", button=" + button + "]";
	}

	
}
