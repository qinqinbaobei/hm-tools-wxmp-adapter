[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/wxmp-user.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22wxmp-user%22)
#### 前言
本组件对应实现微信公众平台“用户管理”章节相关api接口，原接口文档地址：[用户管理](https://developers.weixin.qq.com/doc/offiaccount/User_Management/User_Tag_Management.html)

#### 接口说明
- `top.hmtools.wxmp.user.apis.ITagsApi` 对应 用户标签管理
- `top.hmtools.wxmp.user.apis.IRemarkApi` 对应 设置用户备注名
- `top.hmtools.wxmp.user.apis.IUnionIDApi` 对应 获取用户基本信息(UnionID机制)
- `top.hmtools.wxmp.user.apis.IUserListApi` 对应获取用户列表
- `top.hmtools.wxmp.user.apis.IBlackListApi` 对应 黑名单管理

#### 事件消息类说明
- `top.hmtools.wxmp.user.model.eventMessage` 包对应 获取用户地理位置 消息通知。

#### 使用示例
0. 引用jar包
```
<dependency>
  <groupId>top.hmtools</groupId>
  <artifactId>wxmp-user</artifactId>
  <version>1.0.0</version>
</dependency>
```

1. 获取wxmpSession，参照 [wxmp-core/readme.md](../wxmp-core/readme.md)
```
//2 获取动态代理对象实例
ITagsApi tagsApi = this.wxmpSession.getMapper(ITagsApi.class);

//3 调用微信公众号的创建标签接口
TagWapperParam param = new TagWapperParam();
TagParam tag = new TagParam();
tag.setName("人才");
param.setTag(tag);
TagWapperResult create = tagsApi.create(param);
```
更多示例参见：
- [用户标签管理](src/test/java/top/hmtools/wxmp/user/apis/ITagsApiTest.java)
- [设置用户备注名](src/test/java/top/hmtools/wxmp/user/apis/IRemarkApiTest.java)
- [获取用户基本信息(UnionID机制)](src/test/java/top/hmtools/wxmp/user/apis/IUnionIDApiTest.java)
- [获取用户列表](src/test/java/top/hmtools/wxmp/user/apis/IUserListApiTest.java)
- [获取用户地理位置](src/test/java/top/hmtools/wxmp/user/model/eventMessage/LocationMessageTest.java)
- [黑名单管理](src/test/java/top/hmtools/wxmp/user/apis/IBlackListApiTest.java)


2020.06.01		用户管理组件测试全部 OK
