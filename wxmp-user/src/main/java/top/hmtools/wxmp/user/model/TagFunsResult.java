package top.hmtools.wxmp.user.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

/**
 * Auto-generated: 2019-08-14 15:11:25
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class TagFunsResult extends ErrcodeBean {

	/**
	 * 这次获取的粉丝数量
	 */
	private int count;
	
	/**
	 * 粉丝列表
	 */
	private Data data;
	
	/**
	 * 拉取列表最后一个用户的openid 
	 */
	private String next_openid;

	public void setCount(int count) {
		this.count = count;
	}

	public int getCount() {
		return count;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public Data getData() {
		return data;
	}

	public void setNext_openid(String next_openid) {
		this.next_openid = next_openid;
	}

	public String getNext_openid() {
		return next_openid;
	}

	@Override
	public String toString() {
		return "TagFunsResult [count=" + count + ", data=" + data + ", next_openid=" + next_openid + ", errcode="
				+ errcode + ", errmsg=" + errmsg + "]";
	}

	
}