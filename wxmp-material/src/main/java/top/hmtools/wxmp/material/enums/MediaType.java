package top.hmtools.wxmp.material.enums;

/**
 * 临时素材类型
 * @author HyboJ
 *
 */
public enum MediaType {

	/**
	 * 图片
	 */
	image("image"),
	
	/**
	 * 语音
	 */
	voice("voice"),
	
	/**
	 * 视频
	 */
	video("video"),
	
	/**
	 * 缩略图
	 */
	thumb("thumb"),
	
	/**
	 * 图文消息
	 */
	news("news");
	
	private String name;
	
	private MediaType(String name) {
		this.name=name;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
