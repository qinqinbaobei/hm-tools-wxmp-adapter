package top.hmtools.wxmp.webpage.apis;

import org.junit.Test;

import top.hmtools.wxmp.webpage.BaseTest;
import top.hmtools.wxmp.webpage.jsSdk.JsapiTicketResult;

public class IJsSdkApiTest extends BaseTest{
	
	private IJsSdkApi jsSdkApi;

	/**
	 * 测试 获取js-sdk所需要的ticket信息
	 */
	@Test
	public void testGetTicket() {
		JsapiTicketResult ticket = this.jsSdkApi.getTicket();
		this.printFormatedJson("获取到的js-sdk ticket信息是", ticket);
	}

	@Override
	public void initSub() {
		this.jsSdkApi = this.wxmpSession.getMapper(IJsSdkApi.class);
	}

}
